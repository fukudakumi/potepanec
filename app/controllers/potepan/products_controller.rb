class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCTS_LIMIT_FOR_VIEW = 4
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = Spree::Product.related_products(@product).limit(RELATED_PRODUCTS_LIMIT_FOR_VIEW)
  end
end
