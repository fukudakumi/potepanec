Spree::Product.class_eval do
  scope :includes_price_and_images, -> { includes(master: [:images, :default_price]) }
  # メインの商品を取り除く
  scope :except_product, -> (product) { where.not(id: product.id) }
  # 同じtaxon_idを持つproductを取得
  scope :same_taxons_product, -> (product) {
                                   joins(:taxons).
                                     where(spree_taxons: { id: product.taxons.ids }).
                                     distinct
                                 }
  # メインの商品の関連商品を取得
  scope :related_products, -> (product) {
                                includes_price_and_images.
                                  same_taxons_product(product).
                                  except_product(product)
                              }
end
