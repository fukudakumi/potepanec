Spree::Taxon.class_eval do
  def related_products # rubocop:disable Airbnb/ModuleMethodInWrongFile
    Spree::Product.in_taxon(self)
  end
end
