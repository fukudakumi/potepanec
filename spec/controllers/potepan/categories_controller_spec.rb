require 'rails_helper'
RSpec.describe Potepan::CategoriesController, type: :controller do
  describe 'GET#show' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let(:products) { create_list(:product, 2, taxons: [taxon]) }

    before do
      get :show, params: { id: taxon.id }
    end

    it 'returns status code 200' do
      expect(response.status).to eq 200
    end
    it 'assigns the requested taxonomy to @taxonomies' do
      expect(assigns(:taxonomies)).to match_array taxonomy
    end
    it 'assigns the requested taxon to @taxon' do
      expect(assigns(:taxon)).to eq taxon
    end
    it 'assigns the requested products to @products' do
      expect(assigns(:products)).to eq products
    end
  end
end
