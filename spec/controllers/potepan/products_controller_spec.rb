require 'rails_helper'
RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET#show' do
    let(:product) { create(:product, taxons: [taxon]) }
    let(:taxon) { create(:taxon) }
    let!(:unrelated_product) { create(:product, taxons: [], name: 'Unrelated_product') }

    before do
      get :show, params: { id: product.id }
    end

    it 'returns status code 200' do
      expect(response.status).to eq 200
    end
    it 'assigns the requested product to @product' do
      expect(assigns(:product)).to eq product
    end
    it 'render_template :show' do
      expect(response).to render_template(:show)
    end

    describe '@related_products' do
      context 'when 3 related products exist' do
        let!(:related_products) { create_list(:product, 3, taxons: [taxon]) }

        it 'has 3 related products(to @related_products)' do
          expect(assigns(:related_products)).to match_array related_products
          expect(assigns(:related_products).count).to eq 3
        end
        it 'does not include unrelated product(to @related_products)' do
          expect(assigns(:related_products)).not_to include(unrelated_product)
        end
      end

      context 'when 4 related products exist' do
        let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

        it 'has 4 related products(to @related_products)' do
          expect(assigns(:related_products)).to match_array related_products
          expect(assigns(:related_products).count).to eq 4
        end
        it 'does not include unrelated product(to @related_products)' do
          expect(assigns(:related_products)).not_to include(unrelated_product)
        end
      end

      context 'when 5 related products exist' do
        let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

        it 'has only 4 related products(to @related_products)' do
          expect(assigns(:related_products).count).to eq 4
        end
        it 'does not include unrelated product(to @related_products)' do
          expect(assigns(:related_products)).not_to include(unrelated_product)
        end
      end
    end
  end
end
