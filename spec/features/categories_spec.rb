require 'rails_helper'
RSpec.feature "Categories", :type => :feature do
  describe "display categories details" do
    given(:taxonomy) { create(:taxonomy) }
    given(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id, name: 'Taxon') }
    given!(:taxon_child1) { create(:taxon, taxonomy: taxonomy, parent_id: taxon.id, products: [product1], name: 'Taxon_child1') }
    given!(:taxon_child2) { create(:taxon, taxonomy: taxonomy, parent_id: taxon.id, products: [product2], name: 'Taxon_child2') }
    given!(:product1) { create(:product, price: 19.99) }
    given!(:product2) { create(:product, price: 19.98) }

    feature 'products_area' do
      scenario "display products in taxon_child1 page" do
        visit potepan_category_path(taxon_child1.id)
        expect(page).to     have_current_path(potepan_category_path(taxon_child1.id))
        expect(page).to     have_title taxon_child1.name + " - BIGBAG Store"
        expect(page).to     have_link taxonomy.root.name, href: "javascript:;"
        expect(page).to     have_content product1.name
        expect(page).not_to have_content product2.name
        expect(page).to     have_content product1.display_price
        expect(page).not_to have_content product2.display_price
      end

      scenario "transition to product page and display product" do
        visit potepan_category_path(taxon_child1.id)
        click_on product1.name

        expect(page).to have_current_path(potepan_product_path(product1.id))
        expect(page).to have_title product1.name + " - BIGBAG Store"
        expect(page).to have_content product1.name
        expect(page).to have_content product1.display_price
      end
    end

    feature 'sidebar_area' do
      scenario 'display some taxon links' do
        visit potepan_category_path(taxon_child1.id)
        expect(page).to have_link taxonomy.root.name, href: "javascript:;"

        within('.navbar-side-collapse') do
          expect(page).to have_content taxon.name
          expect(page).to have_link taxon_child1.name
          expect(page).to have_link taxon_child2.name
        end
      end

      scenario 'transition to taxon_child2 page from taxon_child1 page' do
        visit potepan_category_path(taxon_child1.id)
        expect(page).to     have_current_path(potepan_category_path(taxon_child1.id))
        expect(page).to     have_title taxon_child1.name + " - BIGBAG Store"
        expect(page).to     have_link taxonomy.root.name, href: "javascript:;"
        expect(page).to     have_content product1.name
        expect(page).not_to have_content product2.name
        expect(page).to     have_content product1.display_price
        expect(page).not_to have_content product2.display_price

        within('.navbar-side-collapse') do
          click_on taxon_child2.name
        end
        expect(page).to     have_current_path(potepan_category_path(taxon_child2.id))
        expect(page).to     have_title taxon_child2.name + " - BIGBAG Store"
        expect(page).to     have_link taxonomy.root.name, href: "javascript:;"
        expect(page).not_to have_content product1.name
        expect(page).to     have_content product2.name
        expect(page).not_to have_content product1.display_price
        expect(page).to     have_content product2.display_price
      end
    end
  end
end
