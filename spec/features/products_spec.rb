require 'rails_helper'
RSpec.feature "Products", :type => :feature do
  describe "display product details" do
    given(:taxonomy) { create(:taxonomy) }
    given(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    given!(:product) { create(:product, taxons: [taxon]) }
    given!(:related_product) { create(:product, taxons: [taxon], name: 'Related_product') }
    given!(:no_related_product) { create(:product, taxons: [], name: 'NO_related_product', price: 19.98) }

    background do
      visit potepan_product_path(product.id)
    end

    feature 'main_product_area' do
      scenario "display main product details" do
        expect(page).to have_current_path(potepan_product_path(product.id))

        within all('.media-body')[2] do
          expect(page).to have_content product.name
          expect(page).to have_content product.display_price
          expect(page).to have_content product.description
        end
        expect(page).to have_title product.name + " - BIGBAG Store"
      end

      scenario "click 一覧ページへ戻る" do
        expect(page).to have_current_path(potepan_product_path(product.id))
        expect(page).to have_title product.name + " - BIGBAG Store"
        click_link '一覧ページへ戻る'
        expect(page).to have_current_path(potepan_category_path(taxon.id))
        expect(page).to have_title taxon.name + " - BIGBAG Store"
      end
    end

    feature 'related_products_area' do
      scenario "display related products details" do
        expect(page).to have_current_path(potepan_product_path(product.id))

        within('.productBox') do
          expect(page).to     have_content related_product.name
          expect(page).not_to have_content no_related_product.name
          expect(page).to     have_content related_product.display_price
          expect(page).not_to have_content no_related_product.display_price
        end
      end

      scenario "click related products " do
        expect(page).to have_current_path(potepan_product_path(product.id))
        click_link 'Related_product'
        expect(page).to have_current_path(potepan_product_path(related_product.id))

        within('.singleProduct') do
          expect(page).to have_content related_product.name
          expect(page).to have_content related_product.display_price
          expect(page).to have_content related_product.description
        end
        expect(page).to have_title related_product.name + " - BIGBAG Store"
      end
    end
  end
end
