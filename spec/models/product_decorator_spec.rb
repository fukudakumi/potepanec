require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  let(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon], name: 'main_product') }
  let!(:related_product) { create(:product, taxons: [taxon]) }
  let!(:unrelated_product) { create(:product, taxons: []) }

  describe ".except_product scope" do
    it "does not include product passed as arguments" do
      expect(Spree::Product.except_product(product)).not_to include(product)
    end
    it "include other products passed as arguments" do
      expect(Spree::Product.except_product(product)).to contain_exactly(related_product, unrelated_product)
    end
  end

  describe "related_products scope" do
    subject { Spree::Product.related_products(product) }

    it "has related products" do
      is_expected.to contain_exactly(related_product)
    end

    it "does not include unrelated product passed as arguments" do
      is_expected.not_to include(unrelated_product)
    end

    it "does not include product passed as arguments" do
      is_expected.not_to include(product)
    end
  end
end
