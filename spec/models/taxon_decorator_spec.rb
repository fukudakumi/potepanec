require 'rails_helper'

RSpec.describe Spree::Taxon, type: :model do
  let(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }
  let!(:unrelated_product) { create(:product, taxons: []) }

  describe "related_products method" do
    it "has related products" do
      expect(taxon.related_products).to include(related_product)
    end

    it "has no unrelated product" do
      expect(taxon.related_products).not_to include(unrelated_product)
    end
  end
end
